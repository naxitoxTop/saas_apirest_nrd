# Imagen raíz
FROM node

# Carpeta raíz de NODE
WORKDIR /apitechu

# Copia de archivos
ADD . /apitechu

# Añadir volumen
VOLUME ['/logs']

# Exponer Puerto
EXPOSE 3000

# Instalar dependencias
# RUN npm install

# Comando de inicialización
CMD ["npm", "start"]

// Hacemos un include de express
var express = require('express');

// Iniciamos express
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());

// Puerto en el que escuchará express
var port = process.env.PORT || 3000;

// instanciamos la variable para realizar peticiones json
var requestJson = require('request-json');

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechunrd/collections/";
var mLabAPIKey = "apiKey=jyQsonEvBmcvmrRH3HC8DhVUG4UQBy09";

// express escucha el puerto
app.listen(port);

// salida en consola
console.log("API escuchando en el puerto BIP BIP BIP " + port);

// definimos la home de la API
app.get('/apitechu/v1',
  function(req, res){
      console.log("GET /apitechu/v1");
      res.send(
        {
          "msg": "HOLA desde Api Tech U"
        }
      );
  }
);

app.get('/apitechu/v1/users',
  function(req, res){
    console.log("GET /apitechu/v1/users");
    // Do stuff
    res.sendFile('usuarios.json', {root:__dirname});
    //res.sendfile('./usuarios.json'); // Deprecated
  }
);

app.post('/apitechu/v1/users',
  function(req, res){
      console.log('POST /apitechu/v1/users');

      var newUser = {
        "first_name": req.headers.first_name,
        "last_name": req.headers.last_name,
        "country": req.headers.country
      }; // recogemos el nombre y país del usuario a insertar por cabeceras

      var newUser2 = {
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "country": req.body.country
      }; // recogemos el nombre y país del usuario a insertar por el body


      var fichero_users = require('./usuarios.json');
      fichero_users.push(newUser); //añadimos nuevo usuario a la variable
      fichero_users.push(newUser2); //añadimos nuevo usuario a la variable

      writeDataUserToFile(fichero_users, './usuarios.json');

      console.log("Usuario añadido con éxito "+ req.headers.first_name + " " + req.headers.last_name + ' (' + req.headers.country + ')');

      res.send(fichero_users);
  }
);

app.delete('/apitechu/v1/users/:id',
  function(req, res){
      console.log('DELETE /apitechu/v1/users/:id');

      var fichero_users = require('./usuarios.json');
      fichero_users.splice(req.params.id - 1, 1); // como parámetro en la petición (no headers)
      writeDataUserToFile(fichero_users, './usuarios.json');

      console.log("Usuario borrado con éxito");

      res.send(fichero_users);
  }
);

function writeDataUserToFile(data, nombreFichero){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data); // pasamos el fichero json a string

  // Ahora escribimos en el fichero el jsonUserData con el nuevo usuario
  fs.writeFile(nombreFichero,
    jsonUserData,
    "utf-8",
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Fichero de usuarios grabado correctamente");
      }
    }
  );
}

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
      console.log('PARAMETROS');
      console.log(req.params);

      console.log('QUERY STRING');
      console.log(req.query);

      console.log('HEADERS');
      console.log(req.headers);

      console.log('BODY');
      console.log(req.body);

      res.send("PERFECTO");
  }
);

app.post('/apitechu/v1/login',
  function(req, res){
    var email = req.body.email;
    var password = req.body.password;

    var usuarioLogado = 0;

    var users = require('./usuariosPassword.json');

    for(user of users){
      if(user.password==password && email==user.email){
        var msg = {
          "msg" : "Login correcto. Bienvenido " + user.first_name + " " + user.last_name,
          "id"  : user.id
        }
        usuarioLogado = 1;
        user.logged=true;
        res.send(msg);
        break;
      }
    }
    writeDataUserToFile(users, './usuariosPassword.json');
    if(usuarioLogado==0){
      var msg = {
        "msg" : "Login incorrecto."
      }
      res.send(msg);
    }
  }
);

app.post('/apitechu/v1/logout',
  function(req, res){
    var id = req.body.id;
    console.log(id);
    var usuarioLogout=0;

    var users = require('./usuariosPassword.json');
    for(user of users){
      if(user.id==id && user.logged==true){
        var msg = {
          "msg" : "Logout correcto. Adios " + user.first_name + " " + user.last_name,
          "id"  : user.id
        }
        res.send(msg);
        usuarioLogout = 1;
        delete user.logged;
        break;
      }
    }
    writeDataUserToFile(users, './usuariosPassword.json');
    if(usuarioLogout==0){
      var msg = {
        "msg" : "LOGOUT INCORRECTO. Usuario no existe o no estaba logado."
      }
      res.send(msg);
    }
  }
);

app.get('/apitechu/v1/listausuarios',
  function(req, res){
    var users = require('./usuariosPassword.json');
    res.send(users);
  }
);

// v2 users
app.get('/apitechu/v2/users',
  function(req, res){
      console.log("GET /apitechu/v2/users");

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Creado");

      httpClient.get("usuariosPassword?" + mLabAPIKey,
        function(err, resMLab, body){
          console.log(err);
          console.log(body);
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
        }
      );
  }
);

// v2 users con param id
app.get('/apitechu/v2/users/:id',
  function(req, res){
      console.log("GET /apitechu/v2/users/:id");

      var id = req.params.id;
      var query = 'q={"id" : ' + id + '}';

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Creado");

      httpClient.get("usuariosPassword?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
      );
  }
);

// v2 LOGIN email y password
app.post('/apitechu/v2/login',
  function(req, res){
      console.log("GET /apitechu/v2/login");

      var email = req.body.email;
      var password = req.body.password;
      var query = 'q={"email":"'+ email +'","password":"'+ password +'"}';

      var encontrado = false;

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Creado");

      httpClient.get("usuariosPassword?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Login incorrecto."
           }
           //response = false;
           res.status(500);
         } else {
           console.log(body);
           if (body.length > 0) {
             var id = body[0].id;
             console.log(body[0].id);
             var usuarioLogado = body[0].first_name + " - ID = " + id;
             encontrado = true;
           } else {
             response = {
               "msg" : "Usuario-password no encontrado."
             };
             //response = false;
             res.status(404);
             encontrado = false;
           }
         }
         if(encontrado){
           console.log("ENCONTRADO, hacemos el PUT para el LOGIN");
           var queryPut = 'q={"id":'+ id +'}';
           var putBody = '{"$set":{"logged":true}}';
           httpClient.put("usuariosPassword?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                if (errPUT) {
                  response = {
                    "msg" : "No se ha podido settear el LOGGED = TRUE."
                  }
                  //response = false;
                  res.send(response);
                  res.status(500);
                } else {
                  response = {
                    "msg": "LOGIN OK"
                  }
                  //response = true;
                  res.send(response);
                  console.log("LOGIN OK");
                }
              }
           );
         }
       }
      );
  }
);


// v2 LOGIN email y password
app.post('/apitechu/v2/logout',
  function(req, res){
      console.log("GET /apitechu/v2/logout");

      var id = req.body.id;
      var query = 'q={"id":'+ id +',"logged":'+ true +'}';

      var encontrado = false;

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Creado");

      httpClient.get("usuariosPassword?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        var usuarioLogout = "";
        if (err) {
          response = {
            "msg" : "Logout incorrecto."
          }
          //response = false;
          res.status(500);
        } else {
          if (body.length > 0) {
            var id = body[0].id;
            console.log(body[0].id);
            usuarioLogout = body[0].first_name + " - ID = " + id;
            encontrado = true;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            };
            //response = false;
            res.status(404);
            encontrado = false;
          }
          if(encontrado){
            console.log("ENCONTRADO, hacemos el PUT para el LOGOUT");
            var queryPut = 'q={"id":'+ id +'}';
            var putBody = '{"$unset":{"logged":""}}';
            httpClient.put("usuariosPassword?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT){
                 if (errPUT) {
                   response = {
                     "msg" : "No se ha podido settear el LOGGED = FALSE."
                   }
                   //response = false;
                   res.status(500);
                 } else {
                   console.log(resMLabPUT);
                   console.log(bodyPUT);
                   console.log("OK");
                   response = true;
                 }
               }
            );
          }
          console.log("USUARIO LOGOUT: "+usuarioLogout);
          if(usuarioLogout!=""){
            res.send("Usuario logout: "+usuarioLogout);
          }else{
            res.send("Usuario no existe o no estaba logado");
          }
      }
    }
   );
}
);


// v2 users con param id
app.get('/apitechu/v2/users/:id/accounts',
  function(req, res){
      console.log("GET /apitechu/v2/users/:id/accounts");

      var id = req.params.id;
      var query = 'q={"userID" : ' + id + '}';

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Buscando cuenta del userID "+ id);

      httpClient.get("accounts?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta de usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body;
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         console.log(response);
         res.send(response);
       }
      );
  }
);

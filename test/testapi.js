var mocha = require("mocha");
var chai = require("chai");
var chaihttp = require("chai-http");

var server = require('../server.js');

chai.use(chaihttp);

var should = chai.should();

describe('First test',
 function() {
   it('Tests that DuckDuckGo works', function(done) {
     chai.request('http://www.duckduckgo.com')
       .get('/')
       .end(
         function(err, res) {
           console.log("Request has ended");
           console.log(res);
           console.log(err);
           res.should.have.status(200);
           done();
         }
       );
   });
 }
);

describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("HOLA desde Api Tech U")
             done();
           }
         )
     }
   ),
   it('Prueba que la API devuelve una lista de usuarios correctos.',
      function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
          function(err, res) {
              res.should.have.status(200);
              res.body.should.be.a("array");
              for (user of res.body) {
                user.should.have.property('first_name');
                user.should.have.property('email');
              }
              done();
          }
        )
      }
   )
 }
);
